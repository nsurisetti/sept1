class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
def make_list(elements):
	head = ListNode(elements[0])
	for element in elements[1:]:
		ptr = head
		while ptr.next:
			ptr = ptr.next
		ptr.next = ListNode(element)
	return head
def print_list(head):
	ptr = head
	print('[', end = "")
	while ptr:
		print(ptr.val, end = ", ")
		ptr = ptr.next
	print(']')

class Solution:
	def addTwoNumbers(self, l1:ListNode, l2: ListNode) -> ListNode:
		n1 = ""
		n2 = ""
		while l1 != None:
			n1 += str(l1.val)
			l1 = l1.next
		while l2 != None:
			n2 += str(l2.val)
			l2 = l2.next
		res = str(int(n1)+ int(n2))
		nav = flag = ListNode(res[0])
		for e in res:
			nav.next = ListNode(e)
			nav = nav.next
		return flag.next

x = Solution()
list1 = make_list([1,2,1])
list2 = make_list([1,3,2])
print_list(x.addTwoNumbers(list1, list2))
