#given data
student_data = {'id1': 
   {'name': ['Sara'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id2': 
  {'name': ['David'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id3': 
    {'name': ['Sara'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id4': 
   {'name': ['Surya'], 
    'class': ['V'],
   },
}

#empty dictionary to store the result 
resultant_dictionary = {}

#loop over the student_data for unique data and storing them in the empty dictionary taken
for key,value in student_data.items():
    if value not in resultant_dictionary.values():
        resultant_dictionary[key] = value

#printing the resultant_dictionary
print(resultant_dictionary)